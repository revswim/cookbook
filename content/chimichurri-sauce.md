---
title: "Chimichurri Sauce"
date: 2023-02-13
tags: ['sauce', 'argentinian']
author: natalie
---

![Finished chimichurri](/pix/chimichurri-sauce.webp)

In Argentina, chimichurri is used both as a marinade and a sauce for grilled steak, but you can use it also with fish, chicken, lamb, or even pasta. Pairs with [steak gaucho-style](/steak-gaucho-style).

- ⏲️ Prep time: 10 min
- 🍽️ Servings: 4

## Ingredients

- 1 cup lightly packed chopped parsley (ideally, flat leaf "Italian" parsley)
- 3 to 5 cloves garlic, minced
- 1 teaspoon salt
- 1/2 teaspoon freshly ground pepper
- 1/2 teaspoon chili pepper flakes
- 2 tablespoons fresh oregano leaves (optional)
- 2 tablespoons shallot or onion, minced
- 3/4 cup vegetable or olive oil
- 3 tablespoons sherry wine vinegar, or red wine vinegar
- 3 tablespoons lemon juice

## Directions

1. Place all chimichurri sauce ingredients in a blender or food processor and pulse until well chopped, but not pureed. Reserve.
