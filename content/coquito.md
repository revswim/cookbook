---
title: "Coquito"
date: 2023-02-13
tags: ['alcohol', 'christmas', 'drink', 'sweet', 'eggs', 'puertorican']
author: natalie
---

![Finished coquito](/pix/coquito.webp)

Coquito may translate to little coconut, but this boozy drink is big on coconut flavor. Traditionally served around Christmas, this holiday drink originated in Puerto Rico and is made with rum, coconut milk, sweetened condensed milk and spices. It's delicious served cold.

- ⏲️ Prep time: 10 min
- 🍽️ Servings: 8

## Ingredients

- 3 egg yolks
- 15 ounces cream of coconut
- 15 ounces evaporated milk
- 8 ounces condensed milk
- 1/2 pint vanilla ice cream
- 2 cups white rum (recommended: Bacardi)
- 1/2 tablespoon vanilla extract
- 1/2 teaspoon ground cinnamon

## Directions

1. In a large blender combine all ingredients and mix well.
2. Pour into a pitcher and place in the refrigerator for 1 hour. Serve chilled.
