---
title: "Buttermilk Pie"
date: 2023-02-13
tags: ['pie', 'dessert', 'sweet', 'southwest', 'american']
author: david
---

![Finished buttermilk pie](/pix/buttermilk-pie.webp)

This traditionally southern buttermilk pie recipe is simple to make and pleases the whole family. Recipe adapted slightly from the recipe by Maurine Gaskins submitted to a collection printed by the ladies of the Choate Baptist Church. Pairs with [Annelise's pie crust](/annelises-pie-crust).

- ⏲️ Prep time: 30 min
- 🍳 Cook time: 25 min
- 🍽️ Servings: 8

## Ingredients

- 2 x 9-inch unbaked [pie crusts](/annelises-pie-crust)
- 3 eggs
- 2 cups sugar
- 1/4 cup flour
- 1/2 cup melted butter/margarine
- 1 cup buttermilk
- 1 teaspoon vanilla

## Directions

1. Beat eggs slightly and mix in sugar and flour.
2. Add melted butter/margarine and mix well.
3. Stir in buttermilk and vanilla until blended and pour into unbaked pie shell.
4. Bake at 325° until custard is set (usually about 50-60 minutes).
