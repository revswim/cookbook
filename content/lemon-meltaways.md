---
title: "Lemon Meltaways"
date: 2023-02-13
tags: ['lemons', 'cookies', 'snack', 'sweet', 'american']
author: natalie
---

![Finished lemon meltaways](/pix/lemon-meltaways.webp)

Bright and tender Lemon Meltaway Cookies! It’s a simple, buttery, fresh lemon flavored cookie that’s topped with a sweet and vibrant glaze. After one taste they may just become a new favorite!


- ⏲️ Prep time: 25 min
- 🍳 Cook time: 10 min
- 🍽️ Servings: 70

## Ingredients

- 2 1/4 cup buttermilk baking mixer
- 1 1/4 cup confectioners sugars
- 1/4 cup butter (or 1/2 stick)
- 2 1/4 cups granulated sugar
- 3/4 cup flaked coconut
- 6 large eggs
- 1/2 cup lemon juice
- 2 tablespoons lemon peel
- candied peel lemon


## Directions

1. Grease a 15 1/2 - 10 1/2 inch jelly roll pan; set aside.
2. Preheat oven to 350°F
3. In a medium bowl, combine 2 1/4 cups baking mix and 1/4 cup confectionar's sugar.
4. With pastry blender, cut in butter until mixture is crumbly; press into prepared pan.
5. Bake until light brown (15 minutes.)
6. In medium bowl; combine coconut, eggs, 6 tablespoons lemon juice, and the grated lemon peel; pour over baked layer. Bake until set (25 minutes)
7. Loosen edges of pastry from sides of pan while warm.
8. In small bowl, blend remaining confectioner's sugar with remaining lemon juice. Spread glaze over warm filling.
9. When cool, cute into 2 by 1 inch bars. Garnish each with candied lemon peel. Store in airtight container in layers seperated by wax paper.
