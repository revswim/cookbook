---
title: Jalapeno Popper Mushrooms
date: 2023-02-14
tags: ['mushrooms', 'spicy']
author: natalie
---

![Finished jalapeno popper mushrooms](/pix/jalapeno-popper-mushrooms.webp)

My husband loves stuffed jalapenos and I like stuffed mushrooms, so I came up with this recipe that combines the two. Leave some of the seeds or ribs in the jalapeno for more heat! I like to use Neufchatel cheese and turkey bacon for a lower fat version -- it's just as tasty!

- ⏲️ Prep time: 20 min
- 🍳 Cook time: 35 min
- 🍽️ Servings: 8

## Ingredients

- 4 slices bacon
- cooking spray
- 1 teaspoons olive oil
- 16 mushrooms, stems removed and chopped and caps reserved
- 2 clove garlic, minced
- 2 jalapeno pepper, ribs and seeds removed, finely chopped
- 2 x 3 ounce package cream cheese, softened
- 1/4 cup and 2 tablespoons shredded cheddar cheese
- salt and ground black pepper to taste

## Directions

1. Place the bacon in a large, deep skillet, and cook over medium-high heat, turning occasionally, until evenly browned, about 10 minutes. Drain the bacon slices on a paper towel-lined plate. Crumble the bacon slices and set aside.
2. Preheat an oven to 350 degrees F (175 degrees C). Spray a baking dish with cooking spray.
3. Heat the olive oil in a skillet over medium heat. Stir in the chopped mushroom stems, garlic, and jalapeno; cook and stir until the mushrooms release moisture and soften, about 10 minutes. Transfer the mushroom mixture to a bowl, and stir in the cream cheese, cheddar cheese, and bacon. Season with salt and pepper. Spoon the cheese mixture generously into the reserved mushroom caps, and arrange the stuffed caps on the prepared baking dish.
4. Bake in the preheated oven until cheese begins to brown, 15 to 20 minutes.
