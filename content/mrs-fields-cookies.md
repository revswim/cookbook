---
title: "Mrs. Field's Cookies"
date: 2023-02-13
tags: ['chocolate', 'cookies', 'snack', 'sweet', 'american']
author: natalie
---

![Finished Mrs. Field's cookies](/pix/mrs-fields-cookies.webp)

Our family's favorite Oatmeal Chocolate Chip Cookies are soft and chewy, made with a blend of oats and flour, and both white and brown sugar.

- ⏲️ Prep time: 15 min
- 🍳 Cook time: 11 min
- 🍽️ Servings: 30

## Ingredients

- 1 cup butter
- 1 cup granulated sugar
- 1 cup light brown sugar
- 2 large eggs
- 1 teaspoon vanilla extract
- 2 cups all-purpose flour
- 2 1/2 cups old-fashioned rolled oats
- 1/2 teaspoon salt
- 1 teaspoon baking powder
- 1 teaspoon baking soda
- 12 ounce bags chocolate chips (semi-sweet or milk chocolate)

## Directions

1. Preheat oven to 350°F.
2. Line a baking sheet with parchment paper. Set aside.
3. In a large mixing bowl or stand mixer cream together the butter and sugars. Add the eggs and vanilla and mix until smooth.
4. Add the oats to a blender and blend until powdered in texture. In a separate bowl, combine the flour, powdered oats, salt, baking powder, and baking soda.
5. Gently mix the dry ingredients into the wet ingredients. Stir in the chocolate chips.
6. Scoop cookies onto prepared baking sheet. Bake at 350°F for 9-11 minutes (a little longer for larger cookie scoops) or until the tops of the cookies are set (no longer doughy looking). Cookies will harden as they cool, so don't over-bake them. Allow them to cool on the baking sheet for a few minutes before transferring them to a cooling rack to cool completely.

## Additional Notes

Optionally you can add a 4 ounce Hershey Bar grated and 1 1/2 cup chopped nuts (any kind)
