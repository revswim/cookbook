---
title: "Scotcheroos"
date: 2023-02-13
tags: ['chocolate', 'peanut', 'snack', 'sweet', 'dessert', 'american']
author: david
---

![Finished scotcheroos](/pix/scotcheroos.webp)

The original scotcheroos recipe was printed on boxes of Rice Krispies in the 1960’s, and likely got its name from the butterscotch used in the topping. Be aware that even if the name is shortened to scotch bars there is no alcohol in this recipe. Scotcheroos are like a peanut butter rice krispie treat topped with rich melted chocolate and butterscotch. They’re a classic treat that’s chewy, sweet, and irresistible!

- ⏲️ Prep time: 5 min
- 🍳 Cook time: 10 min
- 🍽️ Servings: 20 bars

## Ingredients

- 1 cup sugar
- 1 cup corn syrup
- 1 cup peanut butter
- 6 cups Rice Krispies
- 1 cup semi-sweet chocolate chips
- 1 cup butterscotch chips
- 1 Tablespoon butter

## Directions

1. In a medium sized sauce pan, stir together the sugar and corn syrup. Heat it up until the sugar crystals dissolve but dont let it boil (for long).
2. Remove the sauce pan from the burner and stir the peanut butter in until smooth.
3. Pour the peanut butter mixture over the Rice Krispies and mix with a large spoon. The mixture will be thick and a bit hard to stir. Work quickly as it starts to “set up” as it cools.
4. Dump cereal mixture into a parchment lined 9 x 13 inch pan. Use your hand to gently press it down and get a smooth-ish top. The harder you push, the harder the finished bars will be.
5. Melt the chocolate and butterscotch chips in a double boiler with 1 Tablespoon butter until it is melted.
6. Spread melted chocolate and butterscotch chips over the bars.
