---
title: "Habichuelas Guisadas"
date: 2023-02-14
tags: ['pork', 'potato', 'puertorican']
author: natalie
---

![Finished habichuelas guisada](/pix/habichuelas-guisada.webp)

Carne Guisada is the Puerto Rican version of beef stew, with all the unique flavors that identify our cuisine. It is filling, satisfying, savory, and very high on the list of most-wanted meals. The best part is that the leftovers always taste even better on the following day. So, you might want to make an extra amount!

- ⏲️ Prep time: 10 min
- 🍳 Cook time: 35 min
- 🍽️ Servings: 5

## Ingredients

- 1 can of rosita beans
- 1 packet of Sazon with achiote
- 2 ounces of tomato sauce
- 1 Tablespoon of alcaparrado (olives)
- 1 Tablespoon of olive oil
- 1/4 teaspoon of black pepper
- 1 Tablespoon of Sofrito
- 2 ounces of smoked ham (cut up into bite size pieces)
- salt to taste

## Directions

1. Heat oil in a saucepan.
2. When the oil is hot add sofrito, alcaparrados, sazon, tomato sauce, black pepper and ham. Turn down the heat and simmer for 5 minutes.
3. Add the beans and 2 cups of water. Turn on the heat again to medium and cook for an additional 20 or until the mixture becomes a bit thick but not soupy.

## Addtional Notes

Instead of ham you can use chorizo, about 4 links chopped into bite sizes. Instead of potatoes you can use chunks of calabaza (pumpkin).
