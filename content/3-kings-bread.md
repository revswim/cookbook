---
title: "3 King's Bread"
date: 2023-02-13
tags: ['bread', 'christmas', 'sweet', 'puertorican']
author: natalie
---

![Finished 3 king's bread](/pix/3-kings-bread.webp)

Celebrate Epiphany with friends and family by following our Rosca de Reyes recipe. Rosca de Reyes, or three kings bread, is a traditional sweet yeast bread primarily consumed on January 6th during the Kings Day festivities. Made to resemble a king's crown with candied fruit for the jewels, it has a delicious orange flavor and soft, cake-like texture. Traditionally, a small porcelain figurine that represents baby Jesus is wrapped in wax paper and baked into the kings bread.

- ⏲️ Prep time: 35 min
- 🍳 Cook time: 40 min
- 🍽️ Servings: 8

## Ingredients

- 1 x 1/4-ounce packet active dry yeast
- 1/4 cup warm water
- 1/4 cup dried figs, cut into strips, plus more for garnish
- 1/4 cup candied orange peel, cut into strips, plus more for garnish
- 1/4 cup candied lemon peel, cut into strips, plus more for garnish
- 1/4 cup chopped candied cherries, plus more whole for garnish
- 2 tablespoons light rum
- 1/4 cup milk
- 1/4 cup sugar
- 1/4 cup (1/2 stick) unsalted butter
- 1 teaspoon pure vanilla extract
- 1/4 teaspoon ground cinnamon
- 1 teaspoon salt
- 3 1/2 to 4 cups all-purpose flour
- 3 large eggs, divided
- Water

## Directions

1. In a small bowl, combine the yeast and warm water; stir to blend. Let stand until the yeast comes alive and starts to foam, about 5 to 10 minutes
2. Put all of the candied fruit in small bowl and drizzle the rum on top. Let stand for 15 minutes to 1 hour to infuse the flavor.
3. In a small pot, warm the milk over medium heat. Add the sugar, butter, vanilla, cinnamon, and salt.
4. In a large bowl, mix 3 1/2 cups flour, 2 eggs, yeast mixture, milk mixture, and the rum soaked candied fruits, mixing very well until the dough gathers into a ball. If the dough is too wet, Add additional flour, a little at a time, if needed to form a soft dough. Turn the dough out onto a lightly floured surface and knead until it's smooth and elastic, about 5 minutes. Put the ball of dough back into the bowl and cover with plastic wrap or a kitchen towel and set aside in a warm spot to rise for 1 hour.
5. Remove the dough from the bowl and knead on a lightly floured surface. Using your palms, roll the dough into a long rope. Shape the coil into a ring, sealing the ends together. Insert a little doll or coin into the bread from the bottom, if desired. Line a baking pan with aluminum foil and coat with nonstick cooking spray. Carefully transfer the dough ring to the prepared baking pan.
6. Preheat the oven to 350°F.
7. Beat the remaining egg in a small bowl with 1 tablespoon of water to make an egg wash, and brush the top of the bread. Decoratively garnish the top of the bread with more candied fruit and bake for 35 to 40 minutes until the cake is golden.
8. Cool on a wire rack before slicing.

## Additional Notes

- Let your guests know there is a little doll or coin inserted inside.
