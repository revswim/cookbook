---
title: "Rosa's Hot Sauce"
date: 2023-02-13
tags: ['spicy', 'sauce']
author: natalie
---

This sauce was created for our personal use in the house. Everything we use is fresh and hoome grown in our garden, nothing artificial. Enjoy

- ⏲️ Prep time: 5 min
- ⏲️ Cook time: 10 min
- 🍽️ Servings: 20

## Ingredients

- 1 cup hot peppers
- 8 sweet peppers
- 1/4 cup cilantro or recao
- 4 garlic cloves
- 1 tablespoon vinegar
- All purpose seasoning ([Adobo Goya](/adobo-seasoning))
- Oregano (to taste)
- Water

## Directions

1. Blend all the ingredients together in an electric mixer very well.
2. Place it all into a large pot and cook for 10 minutes. Then reduce or add to make large or small portions
