---
title: "Salt Codfish Fritters"
date: 2023-02-14
tags: ['fish', 'seafood', 'puertorican']
author: natalie
---

![Finished bacalaitos](/pix/salt-codfish-fritters.webp)

Surprise your guests with these crunchy tasty fish flavored fritters that are so popular in the Puerto Rican culture. These bacalaitos are classic Puerto Rican street food found at many kiosks that dot the island’s beachsides. Bacalaitos are normally medium size and fried thin but some individuals prefer them thicker and larger. The key to ensuring you have the best bacalaitos recipe is to soak the codfish overnight in water.

- ⏲️ Prep time: 40 min
- 🍳 Cook time: 15 min
- 🍽️ Servings: 6

## Ingredients

- 1/2 pound salt bacalao, soaked, cooked and shredded
- 2 cups all-purpose flour
- 1 teaspoon salt
- 1/2 teaspoon black pepper
- 1/2 teaspoon baking powder
- 3 garlic cloves, peeled and chopped
- 2 tablespoons basic Recaito
- 1/2 yellow bell pepper, seeded and diced
- 1/2 red bell pepper, seeded and diced
- 2 cups water
- 2 cups corn oil

## Directions

1. Combine the bacalao, flour, salt, pepper, baking powder, garlic, recaito, and bell pepper in a bowl. Add the water and mix well. Let stand at room temperature for 30 minutes.
2. Heat the oil in a frying pan until hot but not smoking. Drop the batter by spoonfuls and fry until golden brown on both sides. Drain on paper towels. Keep the fritters warm in a low oven until you finish frying.
