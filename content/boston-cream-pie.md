---
title: "Boston Cream Pie"
date: 2023-02-13
tags: ['chocolate', 'pie', 'dessert', 'sweet', 'american']
author: natalie
---

![Finished boston cream pie](/pix/boston-cream-pie.webp)

Despite its name, this chocolate-glazed American classic is actually a cake. The name stems from the process that cakes and pies were baked in the same pan. Take a bite from our Boston Cream Pie recipe to taste for yourself.

- ⏲️ Prep time: 30 min
- 🍳 Cook time: 25 min
- 🍽️ Servings: 8

## Cake Ingredients

- 3 large eggs, separated
- 1 tsp vanilla extract
- 1/2 cup granulated sugar
- 3/4 cup cake flour
- pinch salt

## Filling Ingredients

- 1/2 cup granulated sugar
- 1/4 cup all purpose flour
- 1 1/2 cup milk
- 6 large egg yolks
- 2 teaspoons vanilla extract
- pinch salt

## Glaze Ingredients

- 1/2 cup granulated sugar
- 3 tbsp light corn syrup
- 2 tbsp water
- 4 ounces (4 squares) semisweet chocolate, coarsely chopped

## Directions

1. Preheat oven to 350°F. Grease a 9 inch round cake pan. Line with waxed paper
2. Beat together egg yolks and vanilla at medium speed until blended. Beat in half of sugar until very thick and pale.
3. Using clean, dry beaters, beat together egg whites and salt at medium speed until very soft peaks form. Gradually beat in remaining sugar until stiff, but not dry, peaks form.
4. Fold yolk mixture into egg whites. Sift flour over mixture; fold in gently. Do not over mix. Pour batter into prepared pan.
5. Bake cake until top springs back when lightly pressed, 25 minutes. Loosen cake by running metal spatula around sides of pan. Invert cake onto a wire rack. Remove pan, leaving waxed paper on cake. Turn cake right side up. Cool completely on rack.
6. To prepare filling: In a saucepan, mix together sugar and flour. Gradually whisk in milk, then egg yolks, vanilla, and salt. Bring to a boil over medium heat; boil for 1 minute, whisking constantly. Strain through a fine sieve into a bowl. Press plastic wrap on surface. Chill for 30 minutes.
7. Using a serrated knife, cut cake horizontally in half. Carefully remove waxed paper. Place bottom layer on a serving plate. Spread evenly with filling. Top with remaining cake layer.
9. To prepare glaze: In a saucepan, bring sugar, corn syrup, and water to a boil over low heat, stirring constantly, until sugar has dissolved. Remove from heat. Add chocolate and let stand for 1 minute. Whisk until smooth. Gradually pour glaze over cake, allowing it to drip down sides. Let stand until glaze sets.

## Additional Notes

Cream pies can spoil easily so they must be stored in the refrigerator.
