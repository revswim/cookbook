---
title: "Annelise's Pie Crust"
date: 2023-02-13
tags: ['pie', 'american']
author: david
---

![Finished Annelise's pie crust](/pix/annelises-pie-crust.webp)

Simple to make southern pie crust recipe adapted slightly from the recipe by Maurine Gaskins submitted to a collection printed by the ladies of the Choate Baptist Church. Pairs with [buttermilk pie](/buttermilk-pie).

- ⏲️ Prep time: 30 min
- 🍳 Cook time: 25 min
- 🍽️ Servings: 8

## Ingredients

- 2 cups flour
- 2/3 cup shortening
- 1/2 cup ice water
- 2 teaspoons salt

## Directions

1. Mix together flour and salt.
2. Mix in shortening.
3. Add water.
4. Divide in half and form into two balls.
5. Roll each ball flat and fit into two 9-inch pie pans.
