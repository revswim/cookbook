---
title: "Carino's Lemon Butter"
date: 2023-02-14
tags: ['lemons', 'sauce', 'italian', 'garlic']
author: natalie
---

![Finished Carino's lemon butter](/pix/carinos-lemon-butter.webp)

This is for Johnny Carino's Lemon Butter, used in many of their recipies. It can be cut to make less, but it keeps well so if you'll use it, make a full recipe. Pairs with [Carino's jalapeno garlic tilapia](/carinos-jalapeno-garlic-tilapia).

- ⏲️ Prep time: 5 min

## Ingredients

- 1 pound margarine
- 2 pound butter
- 1/4 cup lemon juice
- 1/2 cup white wine
- 2 teaspoon chopped garlic

## Directions

1. Combine these 5 items into a mixing bowl and beat at a medium speed until a smooth consistency in the batter is formed. Once combined, keep refrigerated until needed.
