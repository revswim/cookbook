---
title: "Carino's Jalapeno Garlic Tilapia"
date: 2023-02-14
tags: ['fish', 'seafood', 'italian','garlic']
author: natalie
---

![Finished jalapeno garlic tilapia](/pix/carinos-jalapeno-garlic-tilapia.webp)

Tilapia sautéed in [Carino's lemon butter](/carinos-lemon-butter) sauce. Served with spinach, tomatoes and angel hair pasta.

- ⏲️ Prep time: 20 min
- 🍳 Cook time: 10 min
- 🍽️ Servings: 2

## Ingredients

- 2 X 4 ounce tilapia filets
- 1/2 cup white flour
- 2 ounces melted margarine
- 1 teaspoon chopped garlic
- 1/4 cup diced jalapenos
- 1 cup chopped de-Stemmed spinach
- 1 pinch salt, pepper, garlic Salt
- 4 ounces heavy cream
- 1/4 cup [Carino's lemon butter](/carinos-lemon-butter)
- 5 ounces angel hair pasta
- 1 tspn fresh parsley
- 1/8 cup diced roma tomatoes

## Directions

1. Dredge the tilapia fillets in the flour, and add them with the melted margarine and garlic to a medium temperature sauté pan.
2. Once the tilapia has finished cooking on one side, flip them over and add the jalapenos, spinach, salt, pepper, and garlic salt.
3. Prepare the angel hair in boiling water, and allow it to cook for 3 to 4 minutes depending on the temperature of the water.
4. As the filets begin to flake add the heavy cream, and bring it to a slight boil. Once the cream begins to boil remove the pan from the fire and reduce the [Carino's lemon butter](/carinos-lemon-butter) into the sauce.
5. Place the pasta in a bowl, and pour the fish with the sauce over the pasta. Garnish with fresh roma tomatoes and parsley.

