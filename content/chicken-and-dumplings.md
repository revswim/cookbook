---
title: "Chicken and Dumplings"
date: 2023-02-15
tags: ['chicken', 'dumpling', 'soup', 'american']
author: natalie
---

![Finished chicken and dumplings](/pix/chicken-and-dumplings.webp)

Desc

- ⏲️ Prep time: 20 min
- 🍳 Cook time: 2 hours 30 min
- 🍽️ Servings: 4

## Ingredients

- 1 chicken (3 to 4 pounds), cut into 8 pieces
- 1 teaspoon paprika
- 1/2 teaspoon salt
- freshly ground black pepper, to taste
- 1 tablespoon vegetable oil
- 1 rib celery, cut into 2-inch pieces
- 1 carrot, peeled and cut into 2-inch pieces
- 2 leeks (white part and 1 inch green), well rinsed and cut into 1-inch pieces
- 1 small onion, quartered
- 4 cups Berta’s Chicken Stock (see page 781) or canned broth
- 1 cup unbleached all-purpose flour
- 1 1/2 teaspoons baking powder
- freshly ground black pepper, to taste
- 2 tablespoons chopped fresh parsley
- 2 tablespoons solid vegetable shortening
- 1/3 cup milk
- chopped fresh parsley, for garnish

## Directions

1. Rinse the chicken pieces well, and pat them dry. Combine the paprika, salt, and pepper in a small bowl, and rub this mixture onto the chicken.
2. Heat the oil in a dutch oven, and cook the chicken over high heat until browned, about 10 minutes. Pour off the fat from the pan.
3. Add the vegetables and stock to the chicken, and bring to a boil. Lower the heat, cover, and simmer just until the chicken is tender and cooked through, about 45 minutes. Using a slotted spoon, transfer the chicken and vegetables to a platter and keep warm. Reserve the broth in the pot.
4. Make the dumplings: In a mixing bowl, stir together the flour, baking powder, pepper, and 2 tablespoons parsley. Using a pastry blender, two knives, or your fingertips, cut in the shortening until the mixture resembles coarse crumbs. Then add the milk, stirring just until the mixture is moistened. Gather the dough into a ball, knead it once or twice, and cut it into twelve pieces.
5. Bring the broth back to a simmer, and drop the dumplings into it. Cover, and simmer until they are puffed and cooked through, about 15 minutes.
6. To serve, arrange the chicken, vegetables, and dumplings in four shallow bowls. Ladle some broth into each bowl, and sprinkle with chopped parsley.
