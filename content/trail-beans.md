---
title: "Trail Beans"
date: 2023-02-13
tags: ['beans', 'spicy', 'southwest']
author: natalie
---

- ⏲️ Prep time: 15 min
- ⏲️ Cook time: 6 hours 30 min
- 🍽️ Servings: 8

## Ingredients

- 2 cups (1 pound) dried pinto beans
- 6 cups of water
- 3 ounce piece of lean salt pork, rind removed
- 4 dried hot red chilis about 4 inches long, stemmmed and crushed
- 1 medium onion, sliced thin
- 2 teaspoons salt

## Directions

1. Rinse the beans under cold running water; discard any blemished beans. Put the beans in a large pot and cover with water by 2 inches. Bring to a boil, boiling for 2 mins, remove from heat and let soak 1 hour.
2.  Drain and rinse the beans and return them to the pot. Add the 6 cups of water, the salt pork, the chilis, and the onion. Bring to boil, reduce heat and simmer for 5 hours, covered. Check from time to time, add boiling water if necessary. (Cold water toughens the beans.)
3. Stir in the salt and simmer 30 mins, or until a bean can be easily mashed against the side of the pot. The beans should have absorbed most of the liquid. Serve with a fresh tomato salsa, if desired.
