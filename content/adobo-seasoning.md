---
title: "Adobo Seasoning"
date: 2023-02-13
tags: ['spice', 'filipino']
author: natalie
---

![Finished adobe seasoning](/pix/adobo-seasoning.webp)

A very versatile seasoning blend. Use this as a base seasoning, and build upon it in your dish.
Some versions of Creole seasoning blend contain salt -- I don't do that, because I like to control salt content separately. If you insist on this blend being salty, á la Tony Chachere's "when it's salty enough, it's perfectly seasoned!" rationale, start with 2 tablespoons salt in the blend, increasing up to 4 tablespoons depending on your sodium tolerance level.

- ⏲️ Prep time: 10 min

## Ingredients

- 1 peppercorn (whole black pepper)
- 1/4 teaspoon dried whole oregano
- 1 clove garlic, peeled
- 1 teaspoon salt
- 1/2 teaspoon olive oil
- 1/2 teaspoon vinegar or fresh lime juice

## Directions

1. Crush & mix the following: peppercorn, oregano, garlic.
2. Mix crushed ingredients with the rest of the ingredients.
3. Rub seasoning into meats throughly and set in the refrigerator for several hours.
