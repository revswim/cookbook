---
title: "Mexican Rice"
date: 2023-02-13
tags: ['rice', 'mexican', 'spicy']
author: natalie
---

![Finished mexican rice](/pix/mexican-rice.webp)

A meal wouldn't be Mexican without rice and beans, whether inside or alongside a tortilla. To save time preparing this recipe, use a food processor to chop the onion, garlic, and pepper.

- 🍳 Cook time: 20 min
- 🍽️ Servings: 6-8

## Ingredients

- 1 tablespoon vegetable oil
- 1 1/2 cups white rice
- 1 x 16 ounce Italian plum tomatoes, peeled, chopped, with their juice
- 2 medium onions, chopped
- 2 garlic cloves, crushed
- 1 jalapeno pepper (cored, seeded, and internal ribs removed), chopped
- 1/2 teaspoon cumin
- 2 cups chicken stock, tomato juice, or water
- 1 cup frozen corn kernels (optional)
- 1 cup frozen peas (optional)
- 1/2 cup chopped fresh or frozen carrots (optional)
- fresh parsley or cilantro (optional)

## Directions

1. Heat the oil in a large saucepan and add the rice. Stir until it has turned white, about 3 minutes.
2. Add the tomatoes, onion, garlic, pepper, cumin, liquid, and, if desired, a combination (or all) of the corn, peas, and carrots.
3. Bring to a boil and simmer, covered, until the liquid is absorbed and the rice is tender, about 15 minutes.
4. Garnish with chopped resh parsley or cilantro.
