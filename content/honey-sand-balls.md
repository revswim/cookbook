---
title: "Honey Sand Balls"
date: 2023-02-13
tags: ['cookies', 'snack', 'sweet']
author: natalie
---

![Finished honey sand balls](/pix/honey-sand-balls.webp)

- ⏲️ Prep time: 5 min
- ⏲️ Cook time: 20 min
- 🍽️ Servings: 48

## Ingredients

- 1 cup butter (not margarine), softened
- 1/2 sifted powdered sugar
- 2 tablespoons honey
- 2 cups all-purpose flour
- 3/4 cup chopped walnuts
- 1 teaspoon vanilla
- 1/4 teaspoon salt


## Directions

1. In a large mixing bowl beat together butter, 1/2 cup powdered sugar and honey.
2. Add flour, nuts, vanilla and salt. Mix thoroughly, using hands, if necessary
3. Shape into 1 inch balls. Place 1 1/2 inches apart on a greased cookie sheet.
4. Bake in a 325 oven for 14 to 16 minutes or until the cokies are barely brown.
5. While cookies are still warm, roll them in powdered sugar. Cool Roll cookies in powdered sugar again.
