---
title: "Sofrito"
date: 2023-02-13
tags: ['sauce', 'puertorican']
author: natalie
---

![Finished softrio](/pix/softrio.webp)

Sofrito is the backbone of Puerto Rican flavor. Also referred to as recaito, it’s typically sautéed in oil as the foundation for sauces, braises, beans, stews and rice dishes. It's often the first thing to go into the pan. Ingredients are optional, or can be adjusted for taste.

- ⏲️ Prep time: 10 min
- 🍳 Cook time: 10 min

## Ingredients

- 1 oz salt pork (washed and diced)
- 2 oz lean cured ham (washed and diced)
- 1 tablespoon of lard or vegetable oil
- 1 teaspoon salt
- 1/4 teaspoon whole dried oregano
- 1 onion peeled (washed and finely chopped)
- 1 green pepper, seeded (washed and finely chopped)
- 3 sweet chili peppers, seeded (washed and finely chopped)
- 3 fresh culantro leaves (washed and finely chopped)
- 2 garlic cloves, peeled (washed and finely chopped)

## Directions

1. In a small pan, rapidly brown salt pork and lean cured ham.
2. Reduce the heat to low, add the rest of the ingredients and mix.
3. Saute about 10 mins or until tender, stirring occasionally
