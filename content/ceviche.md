---
title: "Ceviche"
date: 2023-02-13
tags: ['fish', 'seafood', 'peruvian']
author: natalie
---

![Finished ceviche](/pix/ceviche.webp)

Ceviche is a healthy Peruvian dish that is usually served as an appetizer. Fresh fish and other seafood is “cooked” in lime juice and mixed with chilli, coriander/cilantro, onion and other flavourings. Also known as cebiche, seviche or sebiche, this quick and easy recipe makes a wonderful light meal for hot summer days, or an elegant starter.

- ⏲️ Prep time: 15 min
- 🍽️ Servings: 4

## Ingredients

- 12 ounces very fresh (sushi-grade) white-fleshed ocean fish, such as grouper, wahoo, sea bass, or red snapper
- 1/3 cup fresh lime juice
- 3 tablespoons fresh orange juice
- 3 tablespoons pineapple juice
- 1 1/2 tablespoons finely diced serrano pepper
- 2 tablespoons finely diced yellow bell pepper
- 2 tablespoons finely diced red bell pepper
- 1 1/2 tablespoons minced red onion
- 1 teaspoon minced garlic
- 2 tablespoons chopped fresh cilantro leaves
- 1 tablespoon good quality extra-virgin olive oil
- 1/2 teaspoon kosher salt
- 8 x 3-inch plantain chips
- Lime wedges, for serving

## Directions

1. Cut the fish into 1/4-inch dice.
2. Place in a glass dish with the lime juice, orange juice, pineapple juice, peppers, onions, and garlic, tossing to coat.
3. Cover and refrigerate for 3 to 4 hours, stirring occasionally.
4. Add the cilantro, olive oil, and salt. Fold gently to mix.
5. Serve ceviche in martini glasses and garnish with fried plantain chips and lime wedges.
