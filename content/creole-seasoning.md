---
title: "Creole Seasoning"
date: 2023-02-13
tags: ['spice', 'creole']
author: natalie
---

![Finished creole seasoning](/pix/creole-seasoning.webp)

A very versatile seasoning blend. Use this as a base seasoning, and build upon it in your dish.
Some versions of Creole seasoning blend contain salt -- I don't do that, because I like to control salt content separately. If you insist on this blend being salty, á la Tony Chachere's "when it's salty enough, it's perfectly seasoned!" rationale, start with 2 tablespoons salt in the blend, increasing up to 4 tablespoons depending on your sodium tolerance level.

- ⏲️ Prep time: 5 min
- 🍽️ Servings: 20

## Ingredients

- 2 tablespoons onion powder
- 2 tablespoons garlic powder
- 2 tablespoons dried oregano leaves
- 2 tablespoons dried sweet basil
- 1 tablespoon dried thyme leaves
- 1 tablespoon black pepper
- 1 tablespoon white pepper
- 1 tablespoon cayenne pepper
- 1 tablespoon celery seed
- 5 tablespoons sweet paprika

## Directions

1. Combine in food processor and pulse until well-blended, or mix thoroughly in a large bowl.

## Additional Notes

The recipe doubles or triples well. Give lots of it away as gifts to your family and friends.
