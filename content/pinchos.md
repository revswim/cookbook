---
title: "Pinchos"
date: 2023-02-13
tags: ['beef', 'pork', 'chicken', 'steak', 'lamb', 'puertorican']
author: natalie
---

![Finished pinchos](/pix/pinchos.webp)

Pinchos are a popular shish kebab served everywhere in Puerto Rico. They are to Puerto Ricans what Hot Dogs are to Americans. The secret isn't in the sauce, it's in the powder that makes up the marinade.

- ⏲️ Prep time: 5 min
- 🍳 Cook time: 10 min
- 🍽️ Servings: 4

## Ingredients

- 1/2 teaspoon saffron
- 1/4 cup Spanish paprika
- 1/4 cup dried parsley
- 1/4 cup freeze-dried chives
- 2 tablespoons coarse salt (kosher or sea)
- 2 teapoons dried onion flakes
- 2 teapoons dried garlic flakes
- 2 teapoons chilli pepper flakes
- 2 teapoons ground cumin
- 2 teapoons ground coriander
- 2 teapoons dried oregano
- 2 teapoons black pepper

## Directions

1. Blend or grind the powders into a semi fine powder mix.
2. In a mixing bowl sprinkle the powder on 1/2 inch to 1 inch cubes of pork, beef, lamb, chicken, or your choice of meat. Add a little olive oil and marinate the cubes in the refridgeration for 2 to 4 hours minimum (covered)
3. Use bamboo to skewer the meat and grill over high heat, basting with olive oil.
4. Add BBQ sauce of your choice.
