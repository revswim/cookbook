---
title: "Steak Gaucho-Style"
date: 2023-02-13
tags: ['steak', 'beef', 'argentinian']
author: natalie
---

![Finished Steak Gaucho-Style](/pix/steak-gaucho-style.webp)

Pairs with [chimichurri sauce](/chimichurri-sauce).

- ⏲️ Prep time: 20 min
- 🍳 Cook time: 10 min
- 🍽️ Servings: 4

## Ingredients

- 1 tablespoon cayenne pepper
- 3 tablespoons salt
- 2 1/2 pounds rib-eye, New York strip, or sirloin steak, 1 1/2 inches thick
- 2 baguettes, sliced into 1/4-inch-thick slices

## Directions

1. Preheat a grill.
2. Dissolve cayenne pepper and salt in 1 cup hot water. Transfer to a squeeze container.
3. Place the steak directly over a hot grill, baste with the [chimichurri sauce](/chimichurri-sauce), and grill until the outer portion of the meat reaches the desired degree of doneness.
4. Remove the steak from the grill and slice long strips from the outer edges of the steak.
5. Instruct guests to pick up a steak slice from the cutting board with their fingers, place it on a slice of baguette, and enjoy. Return the remaining steak to the grill, baste, and grill until more of the steak is cooked. Remove and repeat the slicing and serving procedure until steak is consumed.


## Addtional Notes

For extra spicy steak, baste 2 or 3 additional times with the cayenne pepper mixture during grilling process. Spoon [chimichurri sauce](/chimichurri-sauce) over steak.
