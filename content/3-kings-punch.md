---
title: "3 King's Punch"
date: 2023-02-13
tags: ['alcohol', 'christmas', 'drink', 'sweet', 'eggs', 'puertorican']
author: natalie
---

![Finished 3 king's punch](/pix/3-kings-punch.webp)

Ponche de los Reyes Magos. Recipe taken from Puerto Rican Cookery by Carmen Aboy Valldejuli.

- ⏲️ Prep time: 0 min
- 🍽️ Servings: 0

## Ingredients

- 1 cup sugar
- 1 cup water
- 1 cinnamon stick
- 1 can (13 oz) evaporated milk, undiluted
- 1 cup white Puerto Rican rum
- 1 cup anisette
- 4 egg yolks
- ground cinnamon (for garnishing)

## Directions

1. Combine sugar, water, and cinnamon stick in a saucepan and bring it to a boil. Remove from heat, discard cinnamon stick, and allow to cool.
2. Gradually mix in evaporated milk, rum, and anisette.
3. Beat egg yolks and fold into mixture. Pour into glass bottle and store in refrigerator.
4. When ready to serve, pour into punch cups and sprinkle with ground cinnamon.
