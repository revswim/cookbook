---
title: "Authentic Texas Chili"
date: 2023-02-13
tags: ['spicy', 'stew', 'beef', 'southwest', 'american']
author: natalie
---

![Finished authentic Texas chili](/pix/authentic-texas-chili.webp)

While you can find all sorts of chili varieties, Texas chili comes with a red base and two strict rules: no beans and no tomatoes. Real Texas chili is all about the beef, chiles and spices. Buckle up because this authentic Texas chili is the most flavorful, perfectly spiced bowl of red you’ve ever tried. Made with a dried chile paste rather than chili powder, every bite of this beefy chili is filled with tender chuck roast in a rich red gravy. This real Texas chili comes with just the right kick and is a guaranteed party pleaser.

- ⏲️ Prep time: 1 hour
- 🍳 Cook time: 2 hours 30 mins
- 🍽️ Servings: 8

## Ingredients

- 8 dried guajillo chiles stemmed and seeds removed
- 1 dried arbol chile stemmed and seeds removed
- 5 cups beef stock divided
- 4 chipotle peppers in adobo sauce seeds left intact
- 2 tablespoons adobo sauce
- 2 tablespoons olive oil
- 4 pounds boneless beef chuck roast cut into 1/2 to 3/4-inch pieces, excess fat removed
- 2 onions diced
- 2 jalapenos seeded and chopped
- 6 garlic cloves minced or grated
- 1 cup strong coffee
- 2 tablespoons apple cider vinegar
- 1 tablespoon cocoa powder
- 1 tablespoon dark brown sugar
- 1 tablespoon ground cumin
- 2 teaspoons smoked paprika
- 1 teaspoon salt
- 1 teaspoon dried Mexican oregano
- 1 teaspoon cayenne pepper
- 1 teaspoon coriander
- 1/2 teaspoon ground cinnamon
- 1/2 teaspoon ground cloves
- 1/2 teaspoon ground allspice

## Directions

1. Over medium-high heat in a large Dutch oven or stock pot, toast the dried chiles until fragrant, about two to three minutes. Add 2 cups beef stock and bring the mixture to a simmer. Cover, reduce heat and continue to simmer for 10-15 minutes until the chiles soften and are pliable.
2. Add the chiles and beef stock used to simmer the chiles to a blender with the chipotle peppers and adobo sauce. Blend until smooth. Set aside.
3. Heat oil in a large stock pot over medium heat. Cook half of the beef chuck until browned, about a few minutes on both sides. Remove from the pot and then repeat with the other half of the beef. Once the second half is browned, remove that beef from the pot as well. Remove excessive drippings so that there's about 2 tablespoons of liquid left in the pot.
4. Add the onion and jalapenos to the pot. Cook until softened, about four to five minutes. Add the garlic and cook for 30 seconds, stirring constantly. Pour in remaining 3 cups of beef stock and strong coffee, scraping up the bottom of the pot with a wooden spoon.
5. Add the beef back to the pot with the dried chile paste. Stir in apple cider vinegar, cocoa powder, dark brown sugar, cumin, smoked paprika, salt, Mexican oregano, cayenne pepper, coriander, cinnamon, cloves and allspice.
6. Bring the chili up to a boil and then lower heat to a simmer. Keep the pot mostly covered with a crack open. Simmer for two to two and a half hours while stirring occasionally. Serve with desired toppings. Enjoy!

![Cooking authentic Texas chili](/pix/authentic-texas-chili2.webp)

## Additional Notes

- If you're sensitive to heat, you might want to leave out the cayenne pepper until the end. Try the chili after cooking and then add cayenne pepper in small increments to taste.
- This chili is even better if it has time to sit in the refrigerator. If possible, I recommend making it at least a day in advance, though it's still good if you need to serve it immediately.
- After cooking, the liquid will reduce a lot. This is normal for Texas chili, which isn't as soupy as some other chilis. If you want it soupier, you can add more beef stock, but I don't really recommend it because you'll dilute the chile flavor. Your call but just consider it a warning.
- You can absolutely freeze this chili in an airtight container.
- Nutritional information is only an estimate. The accuracy of the nutritional information for any recipe on this site is not guaranteed.
